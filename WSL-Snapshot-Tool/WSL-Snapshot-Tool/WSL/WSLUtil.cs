﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSL_Snapshot_Tool.Utils;

namespace WSL_Snapshot_Tool.WSL
{
    internal class WSLUtil
    {
        public delegate void NotifyEvent(object data = null);

        public event NotifyEvent OnFinished;
        public event NotifyEvent OnError;

        internal async Task<WSLInfo> GetWSLInfo()
        {
            string winDir = Environment.GetFolderPath(Environment.SpecialFolder.System);

            string filePath = System.IO.Path.Combine(winDir, "wsl.exe");

            if (!File.Exists(filePath))
                return new WSLInfo();
                
            var info = await ReadWSLData();

            if (OnFinished != null)
                OnFinished(info);

            return info;
        }

        private async Task<WSLInfo> ReadWSLData()
        {
            WSLInfo info = new WSLInfo();

            string tmpPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "temp");
            if (!Directory.Exists(tmpPath))
                Directory.CreateDirectory(tmpPath);

            string cacheFile = System.IO.Path.Combine(tmpPath, Helpers.SanitizeFilename(Guid.NewGuid().ToString()) + ".cache");
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = "/C wsl.exe --list --verbose > " + cacheFile;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.LoadUserProfile = true;
                process.Start();
                process.WaitForExit();

                if (process.ExitCode != 0)
                {
                    if (OnError != null)
                        OnError(process.ExitCode);

                    return info;
                }
            }
            catch (Exception ex)
            {
                if (OnError != null)
                    OnError(ex);

                return info;
            }

            StreamReader cacheReader = new StreamReader(cacheFile);

            info.IsInstalled = true;

            while(cacheReader.Peek() >= 0)
            {
                string ln = await cacheReader.ReadLineAsync();
                ln = ln.Replace("\0", "");
                if (!String.IsNullOrEmpty(ln))
                {
                    if (ln.Contains("STATE") && ln.Contains("NAME") && ln.Contains("VERSION"))
                        continue;

                    string[] splitted = ln.Split(' ');
                    var container = ParseToWSLContainer(splitted);
                    
                    if (container != null)
                    {
                        info.Containers.Add(container);    
                    }
                }
            }

            cacheReader.Close();

            return info;
        }

        private WSLContainer ParseToWSLContainer(string[] splitted)
        {
            string[] values = new string[3];

            try
            {
                int foundValueCounter = 0;

                foreach (string split in splitted)
                {
                    if (split.Trim() == "*")
                        continue;

                    if (String.IsNullOrEmpty(split))
                        continue;

                    values[foundValueCounter++] = split;
                }

                if (values.Length == 3)
                {
                    WSLContainer wSLContainer = new WSLContainer(values[0])
                    {
                        State = values[1],
                        Version = values[2]
                    };
                    return wSLContainer;
                }
                return null;
            }
            catch (Exception ex)
            {
                if (OnError != null)
                    OnError(ex);

                return null;
            }
        }
    }
}
