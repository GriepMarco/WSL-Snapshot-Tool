﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace WSL_Snapshot_Tool.WSL
{
    public class WSLContainer
    {
        private readonly String SnapshotDirectory = Settings.Settings.Load().DefaultSaveLocation;

        public string Name { get; set; }
        public string State { get; set; }
        public string Version { get; set; }


        public delegate void NotifyEvent(object data = null);

        public event NotifyEvent OnFinished;

        public List<WSLSnapshot> Snapshots { get; set; }

        public int SnapshotCount
        {
            get
            {
                return Snapshots.Count;
            }
        }

        public WSLContainer(string Name)
        {
            this.Name = Name;

            Snapshots = new List<WSLSnapshot>();

            if (!Directory.Exists(SnapshotDirectory))
                Directory.CreateDirectory(SnapshotDirectory);

            List<WSLSnapshot> list = new List<WSLSnapshot>();

            foreach (String f in Directory.GetFiles(SnapshotDirectory, "*.json", SearchOption.AllDirectories))
            {
                list.Add(WSL.WSLSnapshot.Load(f));
            }

            this.Snapshots = list.Where(x => x.Distribution == this.Name).ToList();
        }

        public async void CreateSnapshotAsync(string SnapshotName, string SnapshotDescription)
        {
            string targetDir = System.IO.Path.Combine(SnapshotDirectory, this.Name);

            if (!Directory.Exists(targetDir))
                Directory.CreateDirectory(targetDir);

            string targetFile = Path.Combine(targetDir, Guid.NewGuid().ToString());

            WSLSnapshot Snapshot = new WSLSnapshot()
            {
                Distribution = Name,
                DisplayName = SnapshotName,
                Description = SnapshotDescription,
                FileLocation = targetFile
            };

            bool successful = await Snapshot.CreateAsync();  
            
            if (successful)
            {
                Snapshots.Add(Snapshot);
            }

            if (OnFinished != null)
                OnFinished(Snapshot);
        }

        public void Start()
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = "/C wsl.exe -d " + this.Name;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = true;
                process.Start();
            }
            catch (Exception ex)
            {
                //Ignore
            }
        }

        public bool Unregister()
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = "/C wsl.exe --unregister " + this.Name;
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.LoadUserProfile = true;
                process.Start();
                process.WaitForExit();

                return (process.ExitCode == 0);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public override string ToString()
        {
            return Name;
        }

        internal bool SetAsDefault()
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = "/C wsl.exe --setdefault " + this.Name;
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.LoadUserProfile = true;
                process.Start();
                process.WaitForExit();

                return (process.ExitCode == 0);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}