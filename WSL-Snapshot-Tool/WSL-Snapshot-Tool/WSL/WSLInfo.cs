﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSL_Snapshot_Tool.WSL
{
    internal class WSLInfo
    {
        public bool IsInstalled { get; set; }

        public List<WSLContainer> Containers { get; set; }

        public WSLInfo()
        {
            Containers = new List<WSLContainer>();
        }

        public override string ToString()
        {
            if (!IsInstalled)
                return "WSL not found";
            else
                return String.Join("\n", this.Containers);
        }
    }
}
