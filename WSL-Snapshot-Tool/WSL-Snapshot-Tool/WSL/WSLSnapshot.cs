﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WSL_Snapshot_Tool.Utils;

namespace WSL_Snapshot_Tool.WSL
{
    public class WSLSnapshot
    {
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string Distribution { get; set; }
        public string FileLocation { get; set; }
        public DateTime CreatedAt { get; set; }

        public FileInfo SnapshotInfo
        {
            get
            {
                string filename = Helpers.SanitizeFilename(FileLocation) + ".tar";
                if (System.IO.File.Exists(filename))
                {
                    return new FileInfo(filename);
                }
                return null;
            }
        }

        internal async Task<bool> RestoreAsync(bool overrideExisting = false)
        {
            try
            {
                string filePath = Helpers.SanitizeFilename(FileLocation);

                this.CreatedAt = DateTime.Now;

                WSLUtil wsl = new WSLUtil();
                var info = await wsl.GetWSLInfo();

                bool successful = false;

                if (overrideExisting)
                {
                    string fileName = Path.GetFileName(FileLocation);
                    string targetDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "WSL", fileName);

                    try
                    {
                        var instance = info.Containers.Where(x => String.Equals(x.Name, Distribution, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        if (instance == null)
                            return false;


                        if (!instance.Unregister())
                            return false;

                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine(ex2.Message);
                    }

                    successful = Restore(filePath, Distribution, targetDir);
                }
                else
                {
                    string newName = Helpers.SanitizeFilename(String.Concat(Distribution, "_", DisplayName.Trim().Replace(" ", "_")));
                    newName = GetNewWSLContainerName(ref newName, ref info);

                    string fileName = Path.GetFileName(FileLocation);

                    string targetDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "WSL", fileName);

                    if (!Directory.Exists(targetDir))
                        Directory.CreateDirectory(targetDir);

                    successful = Restore(filePath, newName, targetDir);
                }

                return (successful);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static bool Restore(string filePath, string newName, string targetDir)
        {
            Process process = new Process();
            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = "/C echo 'Restoring Snapshot...' && wsl.exe --import " + newName + " \"" + targetDir + "\" " + filePath + ".tar\"";
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.LoadUserProfile = true;
            process.Start();
            process.WaitForExit();

            return process.ExitCode == 0;
        }

        private string GetNewWSLContainerName(ref string targetName, ref WSLInfo info)
        {
            string v = targetName;
            if (info.Containers.Where(x => String.Equals(x.Name, v, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault() != null)
            {
                targetName += "_1";
                return GetNewWSLContainerName(ref targetName, ref info);
            }
            else
            {
                return targetName;
            }
        }

        internal async Task<bool> CreateAsync()
        {
            try
            {
                string filename = Helpers.SanitizeFilename(FileLocation);

                this.CreatedAt = DateTime.Now;

                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = "/C echo 'Creating Snapshot...' && wsl.exe --export " + Distribution + " " + filename + ".tar";
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.LoadUserProfile = true;
                process.Start();
                process.WaitForExit();

                bool successfull = process.ExitCode == 0;

                if (successfull)
                {
                    //Write FileLocation.json
                    JsonHelpers.SaveToJsonFile(filename + ".json", this);
                }

                return (successfull);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static WSLSnapshot Load(String FileName)
        {
            return JsonHelpers.CreateFromJsonFile<WSLSnapshot>(FileName);
        }
    }
}