﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WSL_Snapshot_Tool.WSL;

namespace WSL_Snapshot_Tool.Dialogs
{
    /// <summary>
    /// Interaktionslogik für NewSnapshotDialog.xaml
    /// </summary>
    public partial class NewSnapshotDialog : Window
    {
        public string SnapshotName = "";
        public string SnapshotDescription = "";

        public NewSnapshotDialog(WSLContainer containerInfo)
        {
            InitializeComponent();
            DistriName.Content = containerInfo.Name;
            DistriVersion.Content = containerInfo.Version;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(TextSnapshotName.Text))
            {
                MessageBox.Show("Please enter a Snapshot Name");
                return;
            }
                
            this.SnapshotName = TextSnapshotName.Text;
            this.SnapshotDescription = TextSnapshotDescription.Text;
            this.DialogResult = true;

            this.Close();
        }
    }
}
