﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WSL_Snapshot_Tool.Dialogs
{
    /// <summary>
    /// Interaktionslogik für SettingsDialog.xaml
    /// </summary>
    public partial class SettingsDialog : Window
    {
        private Settings.Settings settings = null;
        public SettingsDialog()
        {
            InitializeComponent();
            this.settings = Settings.Settings.Load();
            RefreshUI();
        }

        private void RefreshUI()
        {
            this.TextBoxDefaultSaveLocation.Text = settings.DefaultSaveLocation;
        }

        private void SaveSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            settings.Save();
            this.Close();
        }

        private void SearchDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                this.settings.DefaultSaveLocation = dialog.SelectedPath;
                RefreshUI();
            }
        }

        private void TextBoxDefaultSaveLocation_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.settings.DefaultSaveLocation = this.TextBoxDefaultSaveLocation.Text;
        }
    }
}
