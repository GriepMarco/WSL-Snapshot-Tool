﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WSL_Snapshot_Tool.WSL;

namespace WSL_Snapshot_Tool.Dialogs
{
    /// <summary>
    /// Interaktionslogik für ShowSnapshotsDialog.xaml
    /// </summary>
    public partial class ShowSnapshotsDialog : Window
    {
        WSLContainer _container;
        public ShowSnapshotsDialog(WSLContainer container)
        {
            InitializeComponent();
            _container = container;
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            WSLSnapshotGrid.ItemsSource = null;
            WSLSnapshotGrid.ItemsSource = _container.Snapshots;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            WSLSnapshot selectedSnapshot = WSLSnapshotGrid.SelectedItem as WSLSnapshot;
            bool enabled = (selectedSnapshot != null);
            RestoreSnapshotButton.IsEnabled = enabled;
            DeleteSnapshotButton.IsEnabled = enabled;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            //Restore
            WSLSnapshot selectedSnapshot = WSLSnapshotGrid.SelectedItem as WSLSnapshot;

            bool overrideInstance = false;

            var result = (MessageBox.Show("Click Yes to create a new instance for the restore. If you click no, we try to unregister the original distri and override with the restore files. Do you want to create a new instance?", "Question", MessageBoxButton.YesNoCancel));

            if (result == MessageBoxResult.Cancel)
                return;

            if (result != MessageBoxResult.Yes)
                overrideInstance = true;

            bool success = await selectedSnapshot.RestoreAsync(overrideInstance);

            if (success)
            {
                MessageBox.Show("Successfully restored snapshot");
                this.Close();
            }
            else
            {
                MessageBox.Show("Failed to restore snapshot");
            }
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            //Delete
            WSLSnapshot selectedSnapshot = WSLSnapshotGrid.SelectedItem as WSLSnapshot;

            string filePath = selectedSnapshot.SnapshotInfo.FullName;

            try
            {
                File.Delete(filePath);
                File.Delete(filePath.Replace(".tar", ".json"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            _container.Snapshots.Remove(selectedSnapshot);
            RefreshGrid();
        }
    }
}
