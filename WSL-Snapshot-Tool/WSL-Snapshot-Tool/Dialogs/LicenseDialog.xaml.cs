﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WSL_Snapshot_Tool.Dialogs
{
    /// <summary>
    /// Interaktionslogik für LicenseDialog.xaml
    /// </summary>
    public partial class LicenseDialog : Window
    {
        private Settings.Settings settings = null;
        public LicenseDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.settings = Settings.Settings.Load();

            this.settings.LicenseSettings = new Settings.LicenseSettings()
            {
                LicenseCode = txtLicenseCode.Text,
                LicenseUser = txtLicenseUser.Text
            };

            if (this.settings.LicenseSettings.IsValid(settings.GetProductID(), settings.GetLicenseServer()))
            {
                this.settings.Save();
                this.DialogResult = true;
                this.Close();
                return;
            }

            MessageBox.Show("Invalid License. Please check your license key or contact support: mail@marcogriep.de", "License invalid");
        }
    }
}
