﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSL_Snapshot_Tool.Settings
{
    public class LicenseEntry
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }

        public string OrderId { get; set; }
        public string ProductId { get; set; }

        public string LicenseKey { get; set; }

        public bool IsValid { get; set; }
    }
}
