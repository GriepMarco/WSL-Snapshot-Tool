﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WSL_Snapshot_Tool.Utils;

namespace WSL_Snapshot_Tool.Settings
{
    public class Settings
    {
        public string DefaultSaveLocation { get; set; }
        private readonly string PRODUCT_ID = "461435";
        private const string LICENSE_SERVER = "https://license.marcogriep.de/api";


        private const string SETTINGS_FILE = "Settings.json";

        public LicenseSettings LicenseSettings { get; set; }

        public static Settings Load()
        {
            string filePath = GetFilePath();

            if (!File.Exists(filePath))
            {
                Settings settings = new Settings();
                settings.DefaultSaveLocation = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "WSL-Snapshots");

                settings.Save();
                return settings;
            }


            Settings loaded =  JsonHelpers.CreateFromJsonFile<Settings>(filePath);
            return loaded;
        }

        
        internal string GetProductID()  
        {
            return this.PRODUCT_ID;
        }

        public void Save()
        {
            string filePath = GetFilePath();

            JsonHelpers.SaveToJsonFile(filePath, this);
        }

        private static string GetFilePath()
        {
            string Path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WSL-Tool");
            if (!Directory.Exists(Path))
                Directory.CreateDirectory(Path);

            return Path + "\\" + SETTINGS_FILE;
        }


        internal string GetLicenseServer()
        {
            return LICENSE_SERVER;
        }
    }
}
