﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WSL_Snapshot_Tool.Settings
{
    public class LicenseSettings
    {
        public string LicenseCode { get; set; }
        public string LicenseUser { get; set; }

        private string LastError { get; set; }

        public bool IsLicensed()
        {
            return (!String.IsNullOrEmpty(LicenseUser) && !String.IsNullOrEmpty(LicenseCode));
        }

        public string GetLastError()
        {
            return this.LastError;
        }

        [System.Runtime.InteropServices.DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public static bool CheckNet()
        {
            int desc;
            return InternetGetConnectedState(out desc, 0);
        }

        public bool IsValid(String ProductId, String LicenseServer)
        {
            if (!IsLicensed())
                return false;

            return Validate(ProductId, LicenseServer);
        }
        private bool Validate(string ProductId, string LicenseServer)
        {
            string url = String.Concat(LicenseServer, "/LicenseEntries/");
            string query = String.Format("{0}/{1}/{2}", this.LicenseUser, ProductId, this.LicenseCode);

            if (!CheckNet())
            {
                string licensePath = GetLicenseStoragePath();
                if (!File.Exists(licensePath))
                {
                    this.LastError = "To activate your license, you will need internet connection.";
                    return false;
                }

                LicenseEntry entry = Utils.JsonHelpers.CreateFromJsonFile<LicenseEntry>(GetLicenseStoragePath());
                if (!String.IsNullOrEmpty(entry.LicenseKey) && !String.IsNullOrEmpty(entry.OrderId))
                {
                    if  ((entry.LicenseKey == this.LicenseCode) && entry.IsValid)
                        return true;
                }
                this.LastError = "No valid license found.";
                return false;
            }

            try
            {
                using (var httpClientHandler = new HttpClientHandler())
                {
                    httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                    var _httpClient = new HttpClient(httpClientHandler);
                    var result = _httpClient.GetAsync(String.Concat(url, query)).Result;

                    string http = result.Content.ReadAsStringAsync().Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        this.LastError = result.StatusCode.ToString() + Environment.NewLine + http;
                        return false;
                    }

                    try
                    {
                        LicenseEntry entry = JsonConvert.DeserializeObject<LicenseEntry>(http);
                        Utils.JsonHelpers.SaveToJsonFile(GetLicenseStoragePath(), entry);
                    }
                    catch { }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.LastError = ex.Message;
                return false;
            }

        }

        private string GetLicenseStoragePath()
        {
            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string targetPath = Path.Combine(localAppData, "WSL-Tool", Environment.MachineName);
            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);

            return String.Concat(targetPath, Path.DirectorySeparatorChar, "data");
        }
    }
}
