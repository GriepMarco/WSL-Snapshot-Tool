﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSL_Snapshot_Tool.Utils
{
    internal static class Helpers
    {
        internal static string SanitizeFilename(string filename)
        {
            return filename.Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "");
        }
    }
}
