﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WSL_Snapshot_Tool.Dialogs;
using WSL_Snapshot_Tool.WSL;

namespace WSL_Snapshot_Tool
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WSLInfo _info;
        DispatcherTimer _timer = new DispatcherTimer();



        private Settings.Settings settings = null;

        public MainWindow()
        {
            InitializeComponent();

            this.settings = Settings.Settings.Load();

            if (settings.LicenseSettings == null)
                settings.LicenseSettings = new Settings.LicenseSettings();

            /*if (!this.settings.LicenseSettings.IsValid(settings.GetProductID(), settings.GetLicenseServer()))
            {
                MessageBox.Show(this.settings.LicenseSettings.GetLastError(), "License Validation", MessageBoxButton.OK, MessageBoxImage.Information);

                LicenseDialog licenseDialog = new LicenseDialog();
                if (licenseDialog.ShowDialog() != true)
                {
                    Environment.Exit(0);
                }
            }*/

            RefreshSnapshotData();
        }

        private void RefreshSnapshotData()
        {
            _info = null;
            _timer.Interval = new TimeSpan(0, 0, 1);
            _timer.Tick += new EventHandler(_timer_Tick); ;
            _timer.Start();

            WSLUtil wsl = new WSLUtil();
            wsl.OnFinished += WSLFinished;
            wsl.OnError += WSLError;
            Task.Factory.StartNew(() => _ = wsl.GetWSLInfo().Result);
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            if (_info is null)
                return;

            if (_info.IsInstalled)
            {
                WSLDataGrid.ItemsSource = null;
                WSLDataGrid.ItemsSource = _info.Containers;
                _timer.Stop();
            }
        }

        private void WSLError(object data)
        {
            MessageBox.Show(data.ToString());
        }

        private void WSLFinished(object data)
        {
            if (data is WSLInfo)
            {
                _info = (WSLInfo)data; 
                if (_info == null && !_info.IsInstalled)
                {
                    MessageBox.Show("Unable to query WSL Information. Is WSL installed?");
                    return;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WSLContainer selectedContainer = WSLDataGrid.SelectedItem as WSLContainer;
            if (selectedContainer == null)
                return;

            selectedContainer.OnFinished += Snapshot_OnFinished;

            NewSnapshotDialog dialog = new NewSnapshotDialog(selectedContainer);
            if (dialog.ShowDialog() == true)
            {
                selectedContainer.CreateSnapshotAsync(dialog.SnapshotName, dialog.SnapshotDescription);
            }
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            WSLContainer selectedContainer = WSLDataGrid.SelectedItem as WSLContainer;
            if (selectedContainer == null)
                return;

            selectedContainer.OnFinished += Snapshot_OnFinished;

            ShowSnapshotsDialog dialog = new ShowSnapshotsDialog(selectedContainer);
            dialog.ShowDialog();

            RefreshSnapshotData();
        }

        private void Snapshot_OnFinished(object data = null)
        {
            RefreshSnapshotData();
        }

        private void OnSelectionChange(object sender, SelectionChangedEventArgs e)
        {
            WSLContainer selectedContainer = WSLDataGrid.SelectedItem as WSLContainer;
            bool enabled = (selectedContainer != null);
        }

        private void OpenSettings(object sender, RoutedEventArgs e)
        {
            SettingsDialog settings = new SettingsDialog();
            settings.ShowDialog();
        }

        private void ManageSnapshotButton_Copy_Click(object sender, RoutedEventArgs e)
        {
            WSLContainer selectedContainer = WSLDataGrid.SelectedItem as WSLContainer;
            if (selectedContainer == null)
                return;

            if (!selectedContainer.Unregister())
            {
                MessageBox.Show("Failed to unregister WSL Instance");
            }

            RefreshSnapshotData();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

            WSLContainer selectedContainer = WSLDataGrid.SelectedItem as WSLContainer;
            if (selectedContainer == null)
                return;

            selectedContainer.Start();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            WSLContainer selectedContainer = WSLDataGrid.SelectedItem as WSLContainer;
            if (selectedContainer == null)
                return;

            selectedContainer.SetAsDefault();
        }

        private async void RestoreFromFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "WSL Backup Definition file (.json|*.json|All Files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                //Restore
                WSLSnapshot selectedSnapshot = WSLSnapshot.Load(openFileDialog.FileName);

                bool overrideInstance = false;

                bool success = await selectedSnapshot.RestoreAsync(overrideInstance);

                if (success)
                {
                    MessageBox.Show("Successfully restored snapshot");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Failed to restore snapshot. Maybe another WSL Instance exists with same name?");
                }
            }
        }
    }
}
