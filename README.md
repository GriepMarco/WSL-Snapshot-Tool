# WSL-Snapshot-Tool

Creating backups for WSL made easy.

Create backups of your WSL instances (Ubuntu, Fedora etc.) and restore them with just a few clicks. My WSL Backup Tool offers you the convenience of a VM snapshot - just for the Windows Subsystem for Linux


## The most important functions

### Easy backup - without command line

With the WSL tool you can create snapshots of the current configuration and restore them if necessary. By supporting versioning of backups, you can flexibly switch between different configuration statuses.

### Clone & migrate WSL instances

Clone your WSL instances and create a copy of your configuration in a new instance. So you can develop different projects in different instances. Copy your WSL instances to another computer and include your WSL installation.

## Need custimization? Dont hestitate to contact me:

- mail@marcogriep.de

Also check out my Webpage: https://www.marcogriep.de

Best Regards
Marco